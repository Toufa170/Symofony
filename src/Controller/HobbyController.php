<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HobbyController extends AbstractController
{
    /**
     * @Route("/hobby", name="hobby")
     * @Route("/hobby/index.html.twig", name="Centre")

     */
    public function index()
    {
        return $this->render('hobby/index.html.twig', [
            'controller_name' => 'HobbyController',
        ]);
    }
}
