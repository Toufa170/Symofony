<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjetsController extends AbstractController
{
    /**
     * @Route("/projets", name="projets")
      * @Route("/projets/index.html.twig", name="Acceuil")

     */
    public function index()
    {
        return $this->render('projets/MesProjets.html.twig', [
            'controller_name' => 'ProjetsController',
        ]);
    }
}
