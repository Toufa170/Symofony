<?php

namespace App\Controller;
use App\Entity\ProjetInfo;
use App\Entity\Module;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MoncontrolleurController extends AbstractController
{
    /**
     * @Route("/moncontrolleur", name="moncontrolleur")
     * @Route("/projets", name="projets")
     * @Route("/hobby/index.html.twig", name="Hobbies")

     * @Route("/moncontrolleur/index.html.twig", name="index")

     */
    public function index()
    {


        $entityManager = $this->getDoctrine()->getManager();
        $ProjetInfoRepository = $entityManager->getRepository(ProjetInfo::class);
        $ProjetInfo = $ProjetInfoRepository->findAll(); 

        if (empty($ProjetInfo)) {
        	  $Projet1 = new ProjetInfo();
        	  $Projet1->getId('HMIN325M');
        	  $entityManager->persist($Projet1);
        	

        	  $Module1 = new Module();
        	  $Module1->getId('HMIN325M');
        	  $Module1->setLanguage(' Projet PHP');
        	  $Module1->setFramework('Symfony');
          	  $Module1->setProjetInfo($Projet1);  
          	  $entityManager->persist($Module1);

          	  $Module2 = new Module();
        	  $Module2->getId('HMIN325M');
        	  $Module2->setLanguage('Projet PHP');
        	  $Module2->setFramework('Angular');
          	  $Module2->setProjetInfo($Projet1);  
          	  $entityManager->persist($Module2);
        	$entityManager->flush();
        }

        return $this->render('moncontrolleur/index.html.twig', [
        	'ProjetInfo' => $ProjetInfoRepository->findAll(),
        ]);
    }
     
    
}
