<?php

namespace App\Repository;

use App\Entity\ProjetInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProjetInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjetInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjetInfo[]    findAll()
 * @method ProjetInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjetInfo::class);
    }

    // /**
    //  * @return ProjetInfo[] Returns an array of ProjetInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjetInfo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
