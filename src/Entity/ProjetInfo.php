<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetInfoRepository")
 */
class ProjetInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Module;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="projetInfo")
     */
    private $Modules;

    public function __construct()
    {
        $this->Modules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModule(): ?string
    {
        return $this->Module;
    }

    public function setModule(?string $Module): self
    {
        $this->Module = $Module;

        return $this;
    }

    /**
     * @return Collection|Module[]
     */
    public function getModules(): Collection
    {
        return $this->Modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->Modules->contains($module)) {
            $this->Modules[] = $module;
            $module->setProjetInfo($this);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->Modules->contains($module)) {
            $this->Modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getProjetInfo() === $this) {
                $module->setProjetInfo(null);
            }
        }

        return $this;
    }
}
