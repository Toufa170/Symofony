<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 */
class Module
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Framework;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProjetInfo", inversedBy="Modules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $projetInfo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->Language;
    }

    public function setLanguage(?string $Language): self
    {
        $this->Language = $Language;

        return $this;
    }

    public function getFramework(): ?string
    {
        return $this->Framework;
    }

    public function setFramework(?string $Framework): self
    {
        $this->Framework = $Framework;

        return $this;
    }

    public function getProjetInfo(): ?ProjetInfo
    {
        return $this->projetInfo;
    }

    public function setProjetInfo(?ProjetInfo $projetInfo): self
    {
        $this->projetInfo = $projetInfo;

        return $this;
    }
}
